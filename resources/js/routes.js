import Threads from './components/Threads';
import Thread from './components/Thread';
export default [
    {
        path: '/',
        name: 'Threads',
        component: Threads,
    },
    {
        path: '/thread/:id',
        name: 'Thread',
        component: Thread,
        props:true,
    },
]
