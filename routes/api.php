<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PassportAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

Route::get('threads-list', [ThreadController::class,'index'])->name('threads-list');
Route::get('thread/show/{thread}', [ThreadController::class,'show'])->name('thread-show');

Route::post('thread/create', [ThreadController::class,'store'])->name('create_thread')->middleware('auth:api');
Route::post('thread/update', [ThreadController::class,'update'])->name('update_thread')->middleware('auth:api');
Route::delete('thread/delete/{id}',[ThreadController::class,'delete'])->name('delete_thread')->middleware('auth:api');

Route::post('comment/create', [CommentController::class,'store'])->name('create_comment')->middleware('auth:api');
Route::put('comment/vote_comment', [CommentController::class,'vote_comment'])->name('vote_comment')->middleware('auth:api');
Route::put('comment/change_visibility', [CommentController::class,'change_visibility'])->name('change_comment_visibility')->middleware('auth:api');
