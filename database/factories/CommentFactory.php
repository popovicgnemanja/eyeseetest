<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $app_user = DB::table('users')
            ->where('email','reddit_user@eyesee.test')->first();

        $number_of_threads = DB::table('threads')->count();

        $thread_id = rand(1,$number_of_threads);

        return [
            'user_id' => $app_user->id,
            'thread_id' => $thread_id,
            'visible' => true,
            'votes' => rand(1,100),
            'comment' => $this->faker->sentence(rand(10,20)),
        ];
    }
}
