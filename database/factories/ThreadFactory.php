<?php

namespace Database\Factories;

use App\Models\Thread;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;


class ThreadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Thread::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $app_user = DB::table('users')
                        ->where('email','reddit_user@eyesee.test')->first();
        return [
            //
            'user_id' => $app_user->id,
            'thread' => $this->faker->sentence('100')
        ];
    }
}
