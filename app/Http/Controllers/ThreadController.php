<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;



class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $threads = Thread::with('user')->get();
        return response()->json($threads, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'thread' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            $thread = Thread::create([
                'user_id' => auth()->user()->id,
                'thread' => $request->thread,
            ]);

            return response()->json(['message' => 'Thread created'], 201);

        } catch (\Exception $exception){
            return response()->json(['error' => 'Error saving thread', 'exception' => $exception], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        //

        return response()->json($thread->load(['user','comments','comments.replies']), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            $thread = Thread::find($request->id);


//            dump($thread->created_at);
//            dump(Carbon::now()->subHour(0));
//            dd($thread->created_at->greaterThan(Carbon::now()->subHour(0)) );
            if($thread->created_at->greaterThan(Carbon::now()->subHour(6)) == false){
                return response()->json(['error' => 'Not allowed to edit this thread, thread is older than 6 hours'], 405);
            }


            if($thread->user_id != auth()->user()->id){
                return response()->json(['error' => 'Not allowed to edit this thread'], 405);
            }

            $validator = Validator::make($request->all(), [
                'thread' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }

            $thread->thread = $request->thread;
            $thread->save();

            return response()->json(['succes' => 'updated'], 200);
        } catch(\Exception $exception) {
            \Log::info('error saving: '. $exception->getMessage());
            return response()->json(['error' => 'Error saving', 'exception' => true], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $thread = Thread::findOrFail($id);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'false', 'message' => 'Thread not Found'], 400);
        }

        if($thread->user_id != auth()->user()->id){
            return response()->json(['error' => 'Not allowed to delete this thread'], 405);
        }

        $result = $thread->delete();
        if($result) {
            $thread_response['result'] = true;
            $thread_response['message'] = "Thread successfully deleted!";
        } else {
            $thread_response['result'] = false;
            $thread_response['message'] = "Thread was not deleted, try again!";
        }

        return json_encode($thread_response, JSON_PRETTY_PRINT);

    }
}
