<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'thread_id' => 'required',
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            $comment = Comment::create([
                'user_id' => auth()->user()->id,
                'thread_id' => $request->thread_id,
                'comment' => $request->comment,
                'parent_comment' => ($request->filled('parent_comment') == true) ? $request->parent_comment : NULL
            ]);

            dd($comment);

            return response()->json(['message' => 'Comment created'], 201);

        } catch (\Exception $exception){
            return response()->json(['error' => 'Error saving thread', 'exception' => $exception], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vote_comment(Request $request){
        try {
            $comment = Comment::findOrFail($request->id);
        } catch (\Exception $exception){
            return response()->json(['result' => 'false', 'message' => 'Comment not found'], 400);
        }

        $comment->increment('votes');
        $comment->save();
        return response()->json(['succes' => 'Your vote has been recorded '], 200);
    }

    public function change_visibility(Request $request){

        try {
            $comment = Comment::findOrFail($request->id);
        } catch (\Exception $exception){
            return response()->json(['result' => 'false', 'message' => 'Comment not found'], 400);
        }

        if($comment->user_id != auth()->user()->id){
            return response()->json(['error' => 'Not allowed to edit this thread'], 405);
        }

        if($comment->visible == 0){
            $comment->visible = 1;
            $message = 'Comment is set to be visible';
        } else {
            $comment->visible = 0;
            $message = 'Comment is set to be not visible';
        }
        $comment->save();

        return response()->json(['succes' => $message ], 200);
    }
}
